(ns ^:figwheel-hooks duplicate-offer-finder.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as r :refer [atom]]
   [clojure.zip :as z]
   [tubax.core :as t]))

(defn get-app-element []
  (gdom/getElement "app"))

(defn update-state
  [state event]
  (.preventDefault event)
  (println (-> event .-target .-value))
  (reset! state (-> event .-target .-value)))

(defn xml-input
  [state]
  [:textarea
    {:rows 20
     :cols 50
     :placeholder "Insert XML here"
     :value @state
     :on-change (partial update-state state)
     }])

(defn process-input
  [xml]
  (let [result (try
                (->> xml
                    t/xml->clj
                    z/xml-zip
                    z/down
                    z/down
                    z/rightmost
                    z/node
                    :content
                    (map :attributes)
                    (map :id)
                    frequencies
                    (filter (fn [[offer-id duplicates]] (> duplicates 1)))
                    (map (fn [[offer-id duplicates]] [:p (str "Offer " offer-id " is mentioned " duplicates " times")])))
                (catch js/Object e
                 [:p (str "Failed to parse XML: " e)]))]
    [:div result]))

(defn main-view []
  (let [state (r/atom "Insert XML here")]
    (fn []
      [:div
       [xml-input state]
       [process-input @state]
      ])))

(defn mount [el]
  (r/render-component [main-view] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
